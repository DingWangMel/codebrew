module.exports = function(Party) {
	Party.upload = function(req, res, material_id, cb) {

        var StorageContainer = Party.app.models.Container;

        StorageContainer.getContainers(function (err, containers) {
            if (containers.some(function(e) { return e.name == Party.partyid; })) {
                StorageContainer.upload(req, res, {container: Party.partyid}, cb);
            }
            else {
                StorageContainer.createContainer({name: Party.partyid}, function(err, c) {
                    StorageContainer.upload(req, res, {container: c.name}, cb);
                });
            }
            cb(null, "success")
        });
    };

    Party.remoteMethod(
        'upload',
        {
         accepts: [
            {arg: 'req', type: 'object'},
            {arg: 'res', type: 'object'}
         ],
         returns: {arg: 'status', type: 'string'}
        }
    );
};
